<?php

namespace App\Service\MyCustomTests;

use GuzzleHttp\Client;

class MyCustomTestUnit extends AbstractMyCustomTest
{
    protected $client;
    protected $apiUrl;
    protected $testMessages;
    protected $repo;

    public function __construct($repo = null)
    {
        $this->repo   = $repo;
        $this->client = new Client(['base_uri' => '/']);
        $this->apiUrl = $this->getServerUrl();
    }

    // Запуск всех сценариев
    public function startTests()
    {
        // $this->testProfile();
        // $this->testCar();
        // $this->testRoundAuction();
        // $this->testLot();
        // $this->testBid();

        // $this->testCarFixture();

        die('test-ok');
    }

}