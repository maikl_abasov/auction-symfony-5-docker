<?php

namespace App\Repository;

use App\Entity\Car;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Car>
 *
 * @method Car|null find($id, $lockMode = null, $lockVersion = null)
 * @method Car|null findOneBy(array $criteria, array $orderBy = null)
 * @method Car[]    findAll()
 * @method Car[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarRepository extends ServiceEntityRepository
{
    use TraitUtilsRepository;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Car::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add($data, bool $flush = true) : int
    {
        $today = date("Y-m-d H:i:s");

        $model = new Car();

        $model->setVin($data["vin"]);
        $model->setRun($data["run"]);
        $model->setYear($data["year"]);
        $model->setPts($data["pts"]);
        $model->setComment($data["comment"]);
        $model->setMarkId($data["mark_id"]);
        $model->setModelId($data["model_id"]);
        $model->setModificationId($data["modification_id"]);
        $model->setGenerationId($data["generation_id"]);
        $model->setStartPrice($data["start_price"]);
        $model->setMaxPrice($data["max_price"]);
        $model->setNds($data["nds"]);
        $model->setCity($data["city"]);
        $model->setPlace($data["place"]);
        $model->setCreatedAt($today);

        if(!empty($data["profile_id"]))
            $model->setProfileId($data["profile_id"]);

        $this->_em->persist($model);
        if ($flush) {
            $this->_em->flush();
            return $model->getId();
        }

        return 0;
    }

    public function getMarksAndModels($auctionType, $formFormat = false){

        $query = "
            SELECT 
                   
               mark.name mark_name,
               mark.id mark_id,    
                   
               model.name model_name,
               model.id model_id,
               model.mark model_mark_id,

               geo.city_id,
               geo.city city_name   
                   
            FROM round_auction round
            INNER JOIN lot ON (lot.round_id = round.id)
            INNER JOIN car ON (car.id = lot.car_id)
            INNER JOIN avc_marks mark ON (mark.id = car.mark_id)
            INNER JOIN avc_models model ON (model.id = car.model_id)
            LEFT JOIN geo_cities geo ON (geo.city_id = car.city)
            WHERE round.start_date > :cur_date
            AND round.auction_type = :auction_type

        ";

        $today = $this->todayDateModify('-1 day');

        $params = ['cur_date' => $today,
                   'auction_type' => $auctionType];

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->prepare($query);
        $resultSet = $stmt->executeQuery($params);
        $result = $resultSet->fetchAllAssociative();

        if((!empty($result)) && $formFormat) {
            $marks = $models = $city = [];
            foreach ($result as $item) {
                $markId = $item['mark_id'];
                $marks[$markId] = $item;
                $modelId = $item['model_id'];
                $models[$modelId] = $item;
                $cityId = $item['city_id'];
                $city[$cityId] = $item;
            }
            $result = ['marks' => $marks, 'models' => $models, 'cities' => $city];
        }

        return $result;
    }

}
