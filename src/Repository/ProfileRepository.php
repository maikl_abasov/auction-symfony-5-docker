<?php

namespace App\Repository;

use App\Entity\Profile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Profile>
 *
 * @method Profile|null find($id, $lockMode = null, $lockVersion = null)
 * @method Profile|null findOneBy(array $criteria, array $orderBy = null)
 * @method Profile[]    findAll()
 * @method Profile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProfileRepository extends ServiceEntityRepository
{
    use TraitUtilsRepository;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Profile::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add($data, bool $flush = true) : int
    {
        $today = date("Y-m-d H:i:s");

        $model = new Profile();
        $model->setEmail($data['email']);
        $model->setPhone($data['phone']);
        $model->setName($data['name']);
        $model->setUserId($data['user_id']);
        $model->setType($data['type']);
        $model->setCreatedAt($today);

        $this->_em->persist($model);
        if ($flush) {
            $this->_em->flush();
            return $model->getId();
        }

        return 0;
    }


    public function convertToArray($items, $one = false)
    {
        $data = [];
        foreach ($items as $item) {
            $elem = [
                'id'     => $item->getId(),
                'name'   => $item->getName(),
                'email'  => $item->getEmail(),
                'phone'  => $item->getPhone(),
                'created_at' => $item->getCreatedAt(),
                'updated_at' => $item->getUpdatedAt(),
                'user_id' => $item->getUserId(),
                'type' => $item->getType(),
                //'role'       => $item->getRole(),
                //'active'     => $item->getActive(),
                //'verify'     => $item->getVerify(),
            ];
            if($one) return $elem;
            $data[] = $elem;
        }
        return $data;
    }


    // /**
    //  * @return Profile[] Returns an array of Profile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Profile
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
