<?php


namespace App\Controller;


use App\Entity\Car;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CarController extends BaseController
{
    protected function getModel() {
        $repo = $this->getRepo(Car::class);
        return $repo;
    }

    /**
     * @Route("/car/create", name="create_car")
     */
    public function create(Request $request): JsonResponse
    {

        $data = $this->transformJsonData($request);

//        $message = (empty($data['password'])) ? 'Не задан пароль': '';
//        $message = (empty($data['email']))    ? 'Не задан email' : '';
//        $message = (empty($data['username'])) ? 'Не задано имя'  : '';
//
//        if($message) {
//            return $this->response(0, false, $message);
//        }

        $item = $this->getItem(['vin' => $data['vin']]);

        if(!empty($item)) {
            $message = 'Авто с таким vin уже существует';
            return $this->response(0, false, $message);
        }

        $repository = $this->getModel();
        $result     = $repository->add($data);
        $status = ($result) ? true : false;

        return $this->response($result, true,'Новый авто добавлен в базу');
    }

}