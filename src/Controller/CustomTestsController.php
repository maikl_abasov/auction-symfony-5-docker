<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Car;
use App\Entity\Lot;
use App\Entity\RoundAuction;

use App\Controller\AuctionController;


/**
 * Class AuctionController
 * @package App\Controller
 *
 * @Route(path="/tests")
 */

class CustomTestsController extends BaseController
{

    /**
     * @Route("/start", name="start_tests")
     */
    public function startTests(Request $request): JsonResponse
    {
        $result = [];

        $lotId = 5;

        $auction = new AuctionController();
        $lotBids = $auction->getLotBidsList($lotId);

        dd($lotBids);

//        $data = $this->transformJsonData($request);
//        $item = $this->getItem(['vin' => $data['vin']]);
//
//        if(!empty($item)) {
//            $message = 'Авто с таким vin уже существует';
//            return $this->response(0, false, $message);
//        }
//
//        $repository = $this->getModel();
//        $result     = $repository->add($data);
//        $status = ($result) ? true : false;

        return $this->response($result, true,'Тесты успешно пройдены');
    }

}