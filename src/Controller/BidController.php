<?php


namespace App\Controller;


use App\Entity\Bid;
use App\Entity\Lot;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class BidController extends BaseController
{

    protected function getModel() {
        $repo = $this->getRepo(Bid::class);
        return $repo;
    }

    protected function fieldsValidate($data) {

        $errors = [];
        $errors[] = (empty($data['lot_id']))     ? 'Не выбран лот': '';
        $errors[] = (empty($data['bid_price']))  ? 'Не задана сумма' : '';
        $errors[] = (empty($data['profile_id'])) ? 'Не задано имя пользователя'  : '';

        foreach ($errors as $message) {
            if(trim($message))
                return $message;
        }
        return false;
    }

    /**
     * @Route("/auction/bid/create", name="create_bid")
     */
    public function setBid(Request $request): JsonResponse
    {
        $data = $this->transformJsonData($request);
        $errMessage = $this->fieldsValidate($data);
        if($errMessage) {
            return $this->response(0, false, $errMessage);
        }

        $bidPrice  = $data['bid_price'];
        $lotId     = $data['lot_id'];
        $priceStep = (!empty($data['price_step'])) ? $data['price_step'] : 5000;

        $repository = $this->getModel();
        $lastBid = $repository->getLastBid($lotId);

        if(!empty($lastBid['price'])) {
            if($bidPrice <= $lastBid['price']) {
                return $this->response(0, false, "Ставка не может быть меньше последней ставки");
            }
        }

        $result     = $repository->setBid($data);
        $status = ($result) ? true : false;

        return $this->response([], $status,'Новый ставка успешна');
    }


    /**
     * @Route("/auction/bid/buy-now", name="buy_now_bid")
     */
    public function buyNow(Request $request): JsonResponse
    {
        $data = $this->transformJsonData($request);
        $errMessage = $this->fieldsValidate($data);
        if($errMessage) {
            return $this->response(0, false, $errMessage);
        }

        // $lotId = $data['lot_id'];

        $lotRepo = $this->getRepo(Lot::class);
        $lotRepo->updateStatus($data, 3);

        $repository = $this->getModel();
        $result = $repository->buyNowBid($data);
        $status = ($result) ? true : false;

        return $this->response([], $status,'Лот выкуплен');
    }

}