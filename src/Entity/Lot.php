<?php

namespace App\Entity;

use App\Repository\LotRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LotRepository::class)
 */
class Lot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $car_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $round_id;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $auction_type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $profile_id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $price_step;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCarId(): ?int
    {
        return $this->car_id;
    }

    public function setCarId(?int $car_id): self
    {
        $this->car_id = $car_id;

        return $this;
    }

    public function getRoundId(): ?int
    {
        return $this->round_id;
    }

    public function setRoundId(?int $round_id): self
    {
        $this->round_id = $round_id;

        return $this;
    }

    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    public function setCreatedAt(?string $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getAuctionType(): ?int
    {
        return $this->auction_type;
    }

    public function setAuctionType(?int $auction_type): self
    {
        $this->auction_type = $auction_type;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPropsToArray(): array
    {
        return (array)get_object_vars($this);
    }

    public function getProfileId(): ?int
    {
        return $this->profile_id;
    }

    public function setProfileId(?int $profile_id): self
    {
        $this->profile_id = $profile_id;

        return $this;
    }

    public function getPriceStep(): ?string
    {
        return $this->price_step;
    }

    public function setPriceStep(?string $price_step): self
    {
        $this->price_step = $price_step;

        return $this;
    }
}
