<?php


namespace App\Entity;


class BaseEntity
{
    public function getPropsToArray(): array
    {
        return (array)get_object_vars($this);
    }
}