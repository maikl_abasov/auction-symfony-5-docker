<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/auction/bid/create' => [[['_route' => 'create_bid', '_controller' => 'App\\Controller\\BidController::setBid'], null, null, null, false, false, null]],
        '/auction/bid/buy-now' => [[['_route' => 'buy_now_bid', '_controller' => 'App\\Controller\\BidController::buyNow'], null, null, null, false, false, null]],
        '/car/create' => [[['_route' => 'create_car', '_controller' => 'App\\Controller\\CarController::create'], null, null, null, false, false, null]],
        '/tests/start' => [[['_route' => 'start_tests', '_controller' => 'App\\Controller\\CustomTestsController::startTests'], null, null, null, false, false, null]],
        '/customer/add' => [[['_route' => 'add_customer', '_controller' => 'App\\Controller\\CustomerController::addCustomer'], null, ['POST' => 0], null, false, false, null]],
        '/customer/get-all' => [[['_route' => 'get_all_customers', '_controller' => 'App\\Controller\\CustomerController::getAllCustomers'], null, ['GET' => 0], null, false, false, null]],
        '/lot-auction/create' => [[['_route' => 'create_lot_auction', '_controller' => 'App\\Controller\\LotController::create'], null, ['POST' => 0], null, false, false, null]],
        '/profile/list/all' => [[['_route' => 'profiles_all', '_controller' => 'App\\Controller\\ProfileController::getList'], null, null, null, false, false, null]],
        '/profile/create' => [[['_route' => 'create_profile', '_controller' => 'App\\Controller\\ProfileController::create'], null, null, null, false, false, null]],
        '/round-auction/create' => [[['_route' => 'create_round_auction', '_controller' => 'App\\Controller\\RoundAuctionController::create'], null, ['POST' => 0], null, false, false, null]],
        '/user/list/all' => [[['_route' => 'users_all', '_controller' => 'App\\Controller\\UserController::getList'], null, null, null, false, false, null]],
        '/user/login' => [[['_route' => 'login_user', '_controller' => 'App\\Controller\\UserController::login'], null, null, null, false, false, null]],
        '/user/create' => [[['_route' => 'create_user', '_controller' => 'App\\Controller\\UserController::create'], null, null, null, false, false, null]],
        '/user/my-custom-tests-start' => [[['_route' => 'custom_tests', '_controller' => 'App\\Controller\\UserController::myCustomTestsStart'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_error/(\\d+)(?:\\.([^/]++))?(*:35)'
                .'|/auction/(?'
                    .'|page/init/([^/]++)(?:/([^/]++))?(*:86)'
                    .'|active\\-(?'
                        .'|rounds/list/([^/]++)(*:124)'
                        .'|auction/(?'
                            .'|list/([^/]++)(*:156)'
                            .'|count/([^/]++)(*:178)'
                        .')'
                    .')'
                    .'|search\\-form/marks/([^/]++)(*:215)'
                    .'|lot/(?'
                        .'|bids\\-list/([^/]++)(*:249)'
                        .'|item/([^/]++)(?:/([^/]++))?(*:284)'
                    .')'
                .')'
                .'|/customer/(?'
                    .'|get/([^/]++)(*:319)'
                    .'|update/([^/]++)(*:342)'
                    .'|delete/([^/]++)(*:365)'
                .')'
                .'|/profile/(?'
                    .'|item(?'
                        .'|/([^/]++)(?:/([^/]++))?(*:416)'
                        .'|s/([^/]++)(?:/([^/]++))?(*:448)'
                    .')'
                    .'|delete/([^/]++)(*:472)'
                .')'
                .'|/round\\-auction/all\\-rounds(?:/([^/]++)(?:/([^/]++))?)?(*:536)'
                .'|/user/(?'
                    .'|item(?'
                        .'|/([^/]++)(?:/([^/]++))?(*:583)'
                        .'|s/([^/]++)(?:/([^/]++))?(*:615)'
                    .')'
                    .'|delete/([^/]++)(*:639)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        35 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        86 => [[['_route' => 'auction_page_init', 'limit' => 12, '_controller' => 'App\\Controller\\AuctionController::auctionPageInit'], ['auction_type', 'limit'], null, null, false, true, null]],
        124 => [[['_route' => 'auction_active_rounds', '_controller' => 'App\\Controller\\AuctionController::getActiveRounds'], ['auction_type'], null, null, false, true, null]],
        156 => [[['_route' => 'auction_active_auction_list', '_controller' => 'App\\Controller\\AuctionController::getActiveAuction'], ['round_id'], null, null, false, true, null]],
        178 => [[['_route' => 'auction_active_auction_count', '_controller' => 'App\\Controller\\AuctionController::getActiveAuctionCout'], ['round_id'], null, null, false, true, null]],
        215 => [[['_route' => 'auction_search_form_marks', '_controller' => 'App\\Controller\\AuctionController::getFormMarks'], ['auction_type'], null, null, false, true, null]],
        249 => [[['_route' => 'auction_lot_bids_list', '_controller' => 'App\\Controller\\AuctionController::getLotBidsList'], ['lot_id'], null, null, false, true, null]],
        284 => [[['_route' => 'auction_lot_item', 'field_name' => 'id', '_controller' => 'App\\Controller\\AuctionController::getLotItem'], ['value', 'field_name'], null, null, false, true, null]],
        319 => [[['_route' => 'get_one_customer', '_controller' => 'App\\Controller\\CustomerController::getOneCustomer'], ['id'], ['GET' => 0], null, false, true, null]],
        342 => [[['_route' => 'update_customer', '_controller' => 'App\\Controller\\CustomerController::updateCustomer'], ['id'], ['PUT' => 0], null, false, true, null]],
        365 => [[['_route' => 'delete_customer', '_controller' => 'App\\Controller\\CustomerController::deleteCustomer'], ['id'], ['DELETE' => 0], null, false, true, null]],
        416 => [[['_route' => 'get_profile', 'fname' => 'id', '_controller' => 'App\\Controller\\ProfileController::selectItem'], ['fvalue', 'fname'], null, null, false, true, null]],
        448 => [[['_route' => 'get_profiles', 'fname' => 'id', '_controller' => 'App\\Controller\\ProfileController::selectItems'], ['fvalue', 'fname'], null, null, false, true, null]],
        472 => [[['_route' => 'delete_profile', '_controller' => 'App\\Controller\\ProfileController::remove'], ['id'], null, null, false, true, null]],
        536 => [[['_route' => 'get_all_rounds_auction', 'auction_type' => 0, 'limit' => 0, '_controller' => 'App\\Controller\\RoundAuctionController::getAllRounds'], ['auction_type', 'limit'], null, null, false, true, null]],
        583 => [[['_route' => 'get_user', 'fname' => 'id', '_controller' => 'App\\Controller\\UserController::selectItem'], ['fvalue', 'fname'], null, null, false, true, null]],
        615 => [[['_route' => 'get_users', 'fname' => 'id', '_controller' => 'App\\Controller\\UserController::selectItems'], ['fvalue', 'fname'], null, null, false, true, null]],
        639 => [
            [['_route' => 'delete_user', '_controller' => 'App\\Controller\\UserController::remove'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
