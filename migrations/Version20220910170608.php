<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220910170608 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE car ADD profile_id INT DEFAULT NULL, ADD color VARCHAR(200) DEFAULT NULL, ADD video VARCHAR(250) DEFAULT NULL');
        $this->addSql('ALTER TABLE car_photo ADD created_at VARCHAR(250) DEFAULT NULL');
        $this->addSql('ALTER TABLE cbn_lots CHANGE status status CHAR(1) DEFAULT NULL, CHANGE pay_block pay_block INT DEFAULT NULL, CHANGE comission comission INT DEFAULT NULL, CHANGE car_class_code car_class_code VARCHAR(1) NOT NULL');
        $this->addSql('ALTER TABLE geo_cities CHANGE city_id city_id INT AUTO_INCREMENT NOT NULL, CHANGE delivery delivery INT NOT NULL');
        $this->addSql('ALTER TABLE lot ADD profile_id INT DEFAULT NULL, ADD price_step VARCHAR(100) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE car DROP profile_id, DROP color, DROP video');
        $this->addSql('ALTER TABLE car_photo DROP created_at');
        $this->addSql('ALTER TABLE cbn_lots CHANGE status status CHAR(1) DEFAULT \'0\', CHANGE pay_block pay_block INT DEFAULT 0, CHANGE comission comission INT DEFAULT 0, CHANGE car_class_code car_class_code VARCHAR(1) DEFAULT \'\' NOT NULL');
        $this->addSql('ALTER TABLE geo_cities CHANGE city_id city_id INT NOT NULL, CHANGE delivery delivery INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE lot DROP profile_id, DROP price_step');
    }
}
