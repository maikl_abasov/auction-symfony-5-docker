<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220910172318 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE car (id INT AUTO_INCREMENT NOT NULL, vin VARCHAR(250) NOT NULL, run VARCHAR(250) DEFAULT NULL, created_at VARCHAR(250) DEFAULT NULL, updated_at VARCHAR(250) DEFAULT NULL, year VARCHAR(250) DEFAULT NULL, pts VARCHAR(250) DEFAULT NULL, comment LONGTEXT DEFAULT NULL, mark_id INT DEFAULT NULL, model_id INT DEFAULT NULL, modification_id INT DEFAULT NULL, generation_id INT DEFAULT NULL, start_price INT DEFAULT NULL, max_price INT DEFAULT NULL, nds VARCHAR(100) DEFAULT NULL, city INT DEFAULT NULL, place VARCHAR(250) DEFAULT NULL, profile_id INT DEFAULT NULL, color VARCHAR(200) DEFAULT NULL, video VARCHAR(250) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE car');
    }
}
