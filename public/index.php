<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Access-Control-Allow-Methods: GET,POST,PUT,DELETE,OPTIONS');
header('Content-Type: text/html; charset=utf-8');

use App\Kernel;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;

require dirname(__DIR__).'/vendor/autoload.php';

(new Dotenv())->bootEnv(dirname(__DIR__).'/.env');

if ($_SERVER['APP_DEBUG']) {
    umask(0000);
    Debug::enable();
}

$kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);

///////////////////////
///
///
// set_error_handler('myCustomErrorHandler', E_ALL);
// register_shutdown_function('myCustomErrorHandler');

//function myCustomErrorHandler(int $errNo, string $errMsg, string $file, int $line) {
//
//    $error = [
//        'err_no'    => $errNo,
//        'message'   => $errMsg,
//        'file'      => $file,
//        'line'      => $line,
//        '$_GET'     => $_GET,
//        '$_REQUEST' => $_REQUEST,
//    ];
//
//    $logData = "Date: " . date('Y.m.d__H:i:s') . "\n";
//    foreach ($error as $fname => $fvalue) {
//        $logData .= " {$fname}: $fvalue \n ";
//    }
//
//    logFileSave($logData . "\n");
//}
//
//function logFileSave($logData, $logFileName = 'my_custom_error_log.txt'){
//    $logFilePath =   __DIR__ . '/../var/log/' . $logFileName;
//    $data = $logData;
//    if(file_exists($logFilePath)) {
//        $current = file_get_contents($logFilePath);
//        $data = $logData . $current;
//    }
//    $save = file_put_contents($logFilePath, $data,  LOCK_EX);
//    return $save;
//}