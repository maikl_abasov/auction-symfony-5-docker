<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{

    protected $client = null;

    protected function sendLocal($url, $method = 'GET', $params = []) {
        if(!$this->client) $this->client = static::createClient();
        $this->client->request($method, $url); // отправляем запрос
        $response = $this->client->getResponse(); // получаем ответ
        return $response;
    }

    protected function createServerUrl($url) {
//        $scheme  = $_SERVER['REQUEST_SCHEME'] . '://';
//        $host    = $_SERVER['SERVER_NAME'];
//        $baseUrl = $_SERVER['BASE'];
//        $serverUrl = $scheme . $host . $baseUrl . $url;
        $serverUrl = 'http://symfony-site/auction-symfony-api/public' . $url;
        return $serverUrl;
    }

    protected function postCurl($url, $data, $method = 'POST') {

        $payload = json_encode($data);
        $headers =  [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($payload)
        ];

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        curl_close($ch);
        // $info = curl_getinfo($ch);
        $info = [];

        return [
            'result' => $result,
            'info' => $info,
        ];
    }

    protected function getUser($userId) {

        // -- /user/item/{fvalue}/{fname}

        $url = '/user/item/' .$userId;
        $response = $this->sendLocal($url);
        $this->assertEquals(200, $response->getStatusCode());

        $content = $response->getContent(); // Достаем тело ответа
        $this->assertJson($content);       // Проверка на Json
        $result = json_decode($content, true);
        $this->assertArrayHasKey('result', $result);

        $result = $result['result']; // получаем из массива результаты
        $this->assertTrue(is_array($result)); // Проверка на true

        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('email', $result);
        $this->assertArrayHasKey('username', $result);

    }

    public function getList()
    {
        $url = '/user/list/all';
        $response = $this->sendLocal($url);

        // Дальше начинаем проверять:
        $this->assertEquals(200, $response->getStatusCode());  // Проверка на код ответ

        $content = $response->getContent(); // Достаем тело ответа
        $this->assertJson($content);       // Проверка на Json
        $result = json_decode($content, true);
        $this->assertArrayHasKey('result', $result);

        $result = $result['result']; // получаем из массива результаты
        $this->assertTrue(is_array($result)); // Проверка на true

        // Проверка массива на наличие в нём определённого ключа
        if(!empty($result[0])) {
            $item = $result[0];
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('email', $item);
            $this->assertArrayHasKey('username', $item);
        }
    }

    public function testRun() {

        $num = 4;
        $newUser = [
            "username" => "TesUserName_" . $num,
            "password" => "123456",
            "email"    => "test{$num}@mail.ru",
            "phone"    => "89056789087"
        ];

        if(!$this->client) $this->client = static::createClient();

        $url = '/user/create';
        $headers = array('CONTENT_TYPE' => 'application/json');

        // Тестируем создание пользователя
        $this->client->request('POST', $url, [], [], $headers, json_encode($newUser));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
        $content = $response->getContent(); // Достаем тело ответа
        $this->assertJson($content);       // Проверка на Json
        $result = json_decode($content, true);
        $this->assertArrayHasKey('result', $result);
        $userId = (!empty($result['result'])) ? $result['result'] : 0;

        // Тестируем проверку на уникальность email (при создании пользователя)
        $this->client->request('POST', $url, [], [], $headers, json_encode($newUser));
        $response = $this->client->getResponse();
        $result = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('Пользователь с таким email уже существует', $result['message']);

        // Тестируем получение пользователя
        $this->getUser($userId);

        // Тестируем список пользователей
        $this->getList();

        // Тестируем авторизацию
        $url = '/user/login';
        $this->client->request('POST', $url, [], [], $headers, json_encode($newUser));
        $response = $this->client->getResponse();
        $result = json_decode($response->getContent(), true);
        $this->assertArrayHasKey('result', $result);
        $this->assertArrayHasKey('token', $result['result']);
        $this->assertArrayHasKey('user', $result['result']);

        // Тестируем удаление пользователя
        $url = '/user/delete/' .$userId;
        $response = $this->sendLocal($url);
        $this->assertEquals(200, $response->getStatusCode());

    }

}